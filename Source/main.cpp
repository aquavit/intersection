//intersect
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

void print( int element )
{
	std::cout << element << " ";
}

int main(int argc, char **argv) 
{ 
    //{ {8,10}, {1,4} {3,6}, {15,10} };
    vector< vector<int> > sourceSampleSet, targetSampleSet;
    vector<int> mergedSample;

    sourceSampleSet.resize( 4 );
    sourceSampleSet[0] = { 8, 10 };
    sourceSampleSet[1] = { 1, 4 };
    sourceSampleSet[2] = { 3, 6 };
    sourceSampleSet[3] = { 15,10 };

	std::cout << "---------------------------" << endl;
	std::cout << "SOURCE SAMPLE SET: " << endl;
    for (vector<int> sample : sourceSampleSet) {
        for_each( sample.begin(), sample.end(), &print);
        std::cout << endl;
    }
    std::cout << "---------------------------" << endl;

    int sourceIndex = 0, referenceIndex = 0;

	int index = 0;
	
	for (vector<int> referenceSample : sourceSampleSet) {
		auto hiloReference = minmax_element(sourceSampleSet[index].begin(), sourceSampleSet[index].end());

		vector<int> intersection;
		for (vector<int> sample : sourceSampleSet) {

			//pull the plug if we enounter "ourself" when riffing through the sample set
			int equality = std::equal(referenceSample.begin(), referenceSample.end(), sample.begin());
			if (equality == 1)
			{
				std::cout << "CONTINUE: reference and comparison samples are equal - step out of loop" << endl;
				continue;
			}

			auto hiloSample = minmax_element(sample.begin(), sample.end());

			std::cout << "ITERATION: comparing reference sample [ "
				<< *hiloReference.first << " " << *hiloReference.second
				<< " ] with sample [ "
				<< *hiloSample.first << " " << *hiloSample.second
				<< " ] " << endl;

			// LOGIC: 
			// Either of these two conditions would need to be true
			// 
			//	Smax > Rmin > Smin 
			//       -- OR --
			//  Smin < Rmax < Smax
			//
			// WHERE:
			// R = reference sample
			// S = sample in set being riffed through
		    //
		    // Reference the simple diagram below that depicts an example intersection case
			//
			// |-----|-----|-----|-----|-----|-----|
			// Rmin        Smin        Rmax        Smax

			if( (*hiloReference.first > *hiloSample.first && *hiloReference.first < *hiloSample.second) ||
				(*hiloReference.second > *hiloSample.first && *hiloReference.second < *hiloSample.second)
				)
			{
				intersection = {
					std::min(*hiloReference.first, *hiloSample.first),
					std::max(*hiloReference.second, *hiloSample.second)
				};

				std::cout << "---------------------------" << endl;
				std::cout << "INTERSECTION: " << endl;
				for_each(intersection.begin(), intersection.end(), &print);
				std::cout << endl;
				std::cout << "---------------------------" << endl;

				if (std::find(targetSampleSet.begin(), targetSampleSet.end(), intersection) == targetSampleSet.end())
				{
					targetSampleSet.push_back(intersection);
				}
			}
			else
			{
				// originally had the "exists" check and sample addition to target vector but would get some false positives

			}
		}

		if (intersection.empty())
		{
			//NB: deferred addition of vector element to ensure all samples in set are riffed through
			if (std::find(targetSampleSet.begin(), targetSampleSet.end(), sourceSampleSet[index]) == targetSampleSet.end())
			{
				targetSampleSet.push_back(sourceSampleSet[index]);
			}
		}
		index++;
	}
	std::cout << "---------------------------" << endl;
    std::cout << "TARGET SAMPLE SET: " << endl;
    for (vector<int> sample : targetSampleSet) {
        for_each( sample.begin(), sample.end(), &print);
        std::cout << endl;
    }
    std::cout << "---------------------------" << endl;

    return 1;
}
