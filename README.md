# Intersection

Here is some code that riffs through a set of samples and will, in the case of intersection, join the intersected samples into a new one.

# Algorithm

The brunt of the algorithm is the use of the minmax_element() function to determine high/low values for a reference vector and then iterating through all of the other vectors to look for intersection with each sample vector.

There needs to be extra consideration to make sure the reference vector is compared to all of the other vectors before making a final decision to capture original (or intersected) vector into the target sample set. The existence of elements in the vector used to capture the intersection case is used to make this decision after one complete riff through.

Worth mentioning is that the use of a target vector of sample vectors consumes more memory than an edit in place of the original vector. So in first pass correctness of "new" target sample set is emphasized over optimization for now.


# Example Output

See below from a build and run cycle in an OSX shell.

The build command line used is in the script build.sh

> clang++ main.cpp -O3 -Wall -Weffc++ -std=c++0x -stdlib=libc++ -o intersection 

The output from the executable running in a terminal in OSX is shown below. 

~~~
TerradactylMacBookPro:Source viking$ ./intersection 
---------------------------
SOURCE SAMPLE SET: 
8 10 
1 4 
3 6 
15 10 
---------------------------
CONTINUE: reference and comparison samples are equal - step out of loop
ITERATION: comparing reference sample [ 8 10 ] with sample [ 1 4 ] 
ITERATION: comparing reference sample [ 8 10 ] with sample [ 3 6 ] 
ITERATION: comparing reference sample [ 8 10 ] with sample [ 10 15 ] 
ITERATION: comparing reference sample [ 1 4 ] with sample [ 8 10 ] 
CONTINUE: reference and comparison samples are equal - step out of loop
ITERATION: comparing reference sample [ 1 4 ] with sample [ 3 6 ] 
---------------------------
INTERSECTION: 
1 6 
---------------------------
ITERATION: comparing reference sample [ 1 4 ] with sample [ 10 15 ] 
ITERATION: comparing reference sample [ 3 6 ] with sample [ 8 10 ] 
ITERATION: comparing reference sample [ 3 6 ] with sample [ 1 4 ] 
---------------------------
INTERSECTION: 
1 6 
---------------------------
CONTINUE: reference and comparison samples are equal - step out of loop
ITERATION: comparing reference sample [ 3 6 ] with sample [ 10 15 ] 
ITERATION: comparing reference sample [ 10 15 ] with sample [ 8 10 ] 
ITERATION: comparing reference sample [ 10 15 ] with sample [ 1 4 ] 
ITERATION: comparing reference sample [ 10 15 ] with sample [ 3 6 ] 
CONTINUE: reference and comparison samples are equal - step out of loop
---------------------------
TARGET SAMPLE SET: 
8 10 
1 6 
15 10 
---------------------------
~~~

The portable C++ code shows (as expected) the same results building and running in Visual Studio 2017 with the hardcoded simple output.

![vs](./VisualStudioDebugger.png)

# APPENDIX


See below for version of clang compiler used in OSX.

~~~
TerradactylMacBookPro:Source viking$ clang -v
Apple LLVM version 9.1.0 (clang-902.0.39.1)
Target: x86_64-apple-darwin17.5.0
Thread model: posix
InstalledDir: /Library/Developer/CommandLineTools/usr/bin
~~~

See below for configuration for Visual Studio, 2017 Community Edition.


~~~
Microsoft Visual Studio Community 2017 
Version 15.6.2
VisualStudio.15.Release/15.6.2+27428.2005
Microsoft .NET Framework
Version 4.7.03056

Installed Version: Community

Visual C++ 2017   00369-60000-00001-AA252
Microsoft Visual C++ 2017

ASP.NET and Web Tools 2017   15.0.40301.0
ASP.NET and Web Tools 2017

C# Tools   2.7.0-beta3-62707-11. Commit Hash: 75dfc9b33ed624dff3985c7435c902c3c58c0e5c
C# components used in the IDE. Depending on your project type and settings, a different version of the compiler may be used.

Common Azure Tools   1.10
Provides common services for use by Azure Mobile Services and Microsoft Azure Tools.

JavaScript Language Service   2.0
JavaScript Language Service

Microsoft JVM Debugger   1.0
Provides support for connecting the Visual Studio debugger to JDWP compatible Java Virtual Machines

Microsoft MI-Based Debugger   1.0
Provides support for connecting Visual Studio to MI compatible debuggers

Microsoft Visual C++ Wizards   1.0
Microsoft Visual C++ Wizards

Microsoft Visual Studio VC Package   1.0
Microsoft Visual Studio VC Package

NuGet Package Manager   4.6.0
NuGet Package Manager in Visual Studio. For more information about NuGet, visit http://docs.nuget.org/.

ProjectServicesPackage Extension   1.0
ProjectServicesPackage Visual Studio Extension Detailed Info

Test Adapter for Boost.Test   1.0
Enables Visual Studio's testing tools with unit tests written for Boost.Test.  The use terms and Third Party Notices are available in the extension installation directory.

Test Adapter for Google Test   1.0
Enables Visual Studio's testing tools with unit tests written for Google Test.  The use terms and Third Party Notices are available in the extension installation directory.

Visual Basic Tools   2.7.0-beta3-62707-11. Commit Hash: 75dfc9b33ed624dff3985c7435c902c3c58c0e5c
Visual Basic components used in the IDE. Depending on your project type and settings, a different version of the compiler may be used.

Visual Studio Code Debug Adapter Host Package   1.0
Interop layer for hosting Visual Studio Code debug adapters in Visual Studio

Visual Studio Tools for CMake   1.0
Visual Studio Tools for CMake
~~~